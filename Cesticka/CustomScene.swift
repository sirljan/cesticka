//
//  CustomScene.swift
//  Cesticka
//
//  Created by Jan Širl on 17/04/16.
//  Copyright © 2016 Jan Širl. All rights reserved.
//

import SpriteKit

import AVKit
import AVFoundation

class CustomScene : SKScene {
    
    var videoNodePlayingClosure:(()->Void)?
    
    //Handling playing SKVideoNode
    func playVideoNode(_ position:CGPoint, size:CGSize, name:String, completion:(()->Void)?) {
        self.videoNodePlayingClosure = {
            if let completion = completion {
                completion()
            }
        }
        let type = "mov"
        guard let path = Bundle.main.path(forResource: name, ofType:type) else {
            debugPrint("Could not find resource \(name).\(type)")
            return
        }
        let playerItem = AVPlayerItem(url: URL(fileURLWithPath: path))
        let player = AVPlayer(playerItem: playerItem)
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd(_:)), name:NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
        let videoNode = SKVideoNode(avPlayer: player)
        videoNode.name = name
        videoNode.zPosition = 100
        videoNode.position = position
        videoNode.size = size
        self.addChild(videoNode)
    }
    
    //When playing SKVideoNode initiliazed with AVPlayerItem
    @objc func playerItemDidReachEnd(_ playerItem:AVPlayerItem) {
        debugPrint("playerItemDidReachEnd")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
        if let closure = self.videoNodePlayingClosure {
            closure()
        }
    }
}
