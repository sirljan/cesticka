//
//  Logger.swift
//  Cesticka
//
//  Created by Jan Širl on 09/04/16.
//  Copyright © 2016 Jan Širl. All rights reserved.
//

import Foundation

let kLogGameScene   = false
let kLogCharacter   = false
let kLogCloud       = false

func logGameScene(_ items: Any...) {
    if kLogGameScene {
        print(items)
    }
}

func logCharacter(_ items: Any..., separator: String = ",", terminator: String = "END") {
    if kLogCharacter {
        print(items)
    }
}

func logCloud(_ items: Any..., separator: String = ",", terminator: String = "END") {
    if kLogCloud {
        print(items)
    }
}

func logError(_ items: Any..., separator: String = ",", terminator: String = "END") {
    //todo log in red
    print(items)
}
