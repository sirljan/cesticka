//
//  Cloud.swift
//  Cesticka
//
//  Created by Jan Širl on 03/04/16.
//  Copyright © 2016 Jan Širl. All rights reserved.
//

import Foundation
import SpriteKit

enum Direction : CGFloat {
    case left = -1
    case right = 1
    
    func oppositeDirection() -> Direction {
        return ((self == .left) ? .right : .left)
    }
}

let kCloudWaitAndGoOtherWay = "kCloudWaitAndGoOtherWay"
let kCloudMove = "kCloudMove"
let kCloudEdgeInset:CGFloat = -300.0

class Cloud: SKSpriteNode {
    var direction:Direction = .right
    var waiting = false
    var velocity:CGFloat = 10;
    
    func switchDirection() {
        logCloud("switchDirection")
        self.removeAllActions()
        
        self.direction = self.direction.oppositeDirection()
        self.waiting = true
        let waitAction = SKAction.wait(forDuration: 1)
        let waitEnded = SKAction.run { 
            self.waiting = false
        }
        self.run(SKAction.sequence([waitAction, waitEnded, self.moveAction()]), withKey: kCloudWaitAndGoOtherWay)
    }
    
    func moveAction() -> SKAction {
        let moveAction = SKAction.repeatForever(SKAction.moveBy(x: self.velocity * self.direction.rawValue, y: 0, duration: 1))
        return moveAction
    }
    
    func updateMovement() {
        guard let cameraPositionX = self.scene?.camera?.position.x else {
            logError("ERROR: Unknown position of camera")
            return
        }
        let positionOfCamera = self.position.x - cameraPositionX < 0 ? Direction.right : Direction.left
        let goingTowardsCamera = !self.waiting && self.direction == positionOfCamera
        
        guard let scene = self.scene else {
            logError("ERROR: No scene")
            return
        }
        guard let camera = self.scene?.camera else {
            logError("ERROR: No camera")
            return
        }
        let cameraFrame = CGRect(x: camera.position.x, y: camera.position.y, width: scene.frame.size.width * camera.xScale, height: scene.frame.size.height * camera.yScale)
        let cameraInsets = UIEdgeInsetsMake(0, kCloudEdgeInset, 0, kCloudEdgeInset)
        let extendedCameraFrame = UIEdgeInsetsInsetRect(cameraFrame, cameraInsets)
        let isCloseToCamera = extendedCameraFrame.contains(self.frame)
        if let isVisible = self.scene?.camera?.contains(self) {
            if isVisible {
                logCloud("isVisible")
                if self.waiting {
                    self.removeAction(forKey: kCloudWaitAndGoOtherWay)
                    self.run(self.moveAction(), withKey: kCloudMove)
                }
            } else {
                logCloud("not Visible")
                if goingTowardsCamera {
                    //keep going
                } else {
                    if isCloseToCamera {
                        logCloud("isCloseToCamera")
                        if self.waiting {
                            //keep waiting
                        } else {
                            self.switchDirection()
                        }
                    } else {
                        logCloud("far from camera")
                        //don't wait and move right towards camera
                        self.removeAction(forKey: kCloudWaitAndGoOtherWay)
                        self.run(self.moveAction(), withKey: kCloudMove)
                    }
                }
            }
        }
    }
}
