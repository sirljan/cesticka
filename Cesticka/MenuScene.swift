//
//  MenuScene.swift
//  Cesticka
//
//  Created by Jan Širl on 09/04/16.
//  Copyright © 2016 Jan Širl. All rights reserved.
//

import Foundation
import SpriteKit

let kSelectionBumpActionName = "selectionBump"
let kSelectionBumpRelativeActionName = "selectionBumpRelative"

class MenuScene: SKScene {
    
    override func didMove(to view: SKView) {
        print("MenuScene didMoveToView")
        
        //unhide launchscreen from sks so it could be animated with alpha
        if let launchScreen = self.childNode(withName: "launchScreen") {
            launchScreen.zPosition = 1000
        }
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGestureRecognizer(_:)))
        self.view!.addGestureRecognizer(gestureRecognizer)
        

        self.presentCharacters()
    }
    
    func presentCharacters() {
        
        //hide them first
        self.enumerateChildNodes(withName: "//character*") { (character, boolPointer) in
            character.alpha = 0
        }
        
        //animate launchScreen alpha
        if let launchScreen = self.childNode(withName: "launchScreen") {
            let disappear = SKAction.fadeAlpha(to: 0, duration: 0.5)
            launchScreen.run(disappear, completion: { 
                launchScreen.isHidden = true
                launchScreen.removeFromParent()
                
                //unhide them
                self.enumerateChildNodes(withName: "//character*", using: { (character, boolPointer) in
                    character.run(SKAction.fadeAlpha(to: 1, duration: 0.33))
                })
            })
        }
    }
    
    
    @objc func handleTapGestureRecognizer(_ recognizer: UITapGestureRecognizer) {
        if recognizer.state == .ended {
            let touchLocationView = recognizer.location(in: recognizer.view)
            let touchLocationScene = self.convertPoint(fromView: touchLocationView)
            
            if let boy = self.childNode(withName: "//characterBoy"), let girl = self.childNode(withName: "//characterGirl") {
                var tappedCharacter:SKNode?
                var otherCharacter:SKNode?
                var characterType:CharacterType?
                var touchLocation = self.convert(touchLocationScene, to: girl.parent!)
                if girl.frame.contains(touchLocation) {
                    tappedCharacter = girl
                    otherCharacter = boy
                    characterType = .Girl
                }
                touchLocation = self.convert(touchLocationScene, to: boy.parent!)
                if boy.frame.contains(touchLocation) {
                    tappedCharacter = boy
                    otherCharacter = girl
                    characterType = .Boy
                }
                
                let scaleUp = SKAction.scale(to: 1.2, duration: 0.1)
                scaleUp.timingMode = .easeIn
                let scaleDown = SKAction.scale(to: 1, duration: 0.1)
                scaleDown.timingMode = .easeOut
                let selection = SKAction.sequence([scaleUp, scaleDown])
                
                let disappear = SKAction.fadeAlpha(to: 0, duration: 0.33)
                let done = SKAction.run({
                    otherCharacter?.removeFromParent()
                    if let characterType = characterType {
                        self.transitionToFlatScene(characterType)
                    }
                })
                
                tappedCharacter?.run(selection)
                otherCharacter?.run(SKAction.sequence([disappear, done]))
            }
        }
    }
    
    func transitionToFlatScene(_ characterType: CharacterType) {
        if let scene = FlatScene(fileNamed:"FlatScene") {
            scene.characterType = characterType
            // Configure the view.
            if let skView = self.view {
                skView.showsFPS = true
                skView.showsNodeCount = true
                
                /* Sprite Kit applies additional optimizations to improve rendering performance */
                skView.ignoresSiblingOrder = true
                
                /* Set the scale mode to scale to fit the window */
                scene.scaleMode = .aspectFit
                let transition = SKTransition.doorsOpenHorizontal(withDuration: 1)
//                transition.pausesIncomingScene = true
                NSLog("presents FlatScene")
                skView.presentScene(scene, transition: transition)
            }
        }
    }
}
