//
//  Character.swift
//  Cesticka
//
//  Created by Jan Širl on 20/03/16.
//  Copyright © 2016 Jan Širl. All rights reserved.
//

import SpriteKit

enum CharacterType:String {
    case Girl = "_girl"
    case Boy = "_boy"
}

enum WalkingPart:String {
    case Start = "halfstepStart"
    case Mid = "doublestep"
    case End = "halfstepEnd"
}

enum WalkingDirection:CGFloat {
    case left = -1
    case facingUs = 0
    case right = 1
    
    func opposite() -> WalkingDirection {
        switch self {
        case .left:
            return .right
        case .right:
            return .left
        case .facingUs:
            return .facingUs
        }
    }
    
    func toString() -> String {
        switch self {
        case .left:
            return "WalkingDirection.Left"
        case .facingUs:
            return "WalkingDirection.FacingUs"
        case .right:
            return "WalkingDirection.Right"
        }
    }
}

enum MoveMode:CGFloat {
    case normal = 128
    case fast = 256
}

let kTimePerFrame = 0.1
let kBeginningOfMovingInHalfStepStart = 5
let kStartMovingDistance = 40.0
let kCharStartUpOffset = CGPoint(x: 200, y: 233)

let kStartWalking = "startWalkingSequence"
let kDoubleStepWalking = "doubleStepWalkingReapeatedAction"
let kMovingAction = "movingAction"
let kEndWalking = "endWalkingAction"

class Character : SKSpriteNode {
    internal var characterType:CharacterType = .Girl
    internal let velocity:MoveMode = MoveMode.normal
    fileprivate var currentDirection:WalkingDirection = .facingUs
    
    fileprivate var walkingClosure:(()->Void)?
    
    fileprivate var newStuffBlock:(() -> Void)?
    
    init(characterType:CharacterType, startupOffsetX:CGFloat) {
        self.characterType = characterType
        let texture = SKTexture(imageNamed: "\(WalkingPart.Start.rawValue)1\(characterType.rawValue)")
        super.init(texture: texture, color: UIColor.red, size: texture.size())
        self.name = "character"
        self.position = CGPoint(x: startupOffsetX + kCharStartUpOffset.x, y: kCharStartUpOffset.y)
        self.zPosition = 50
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func walkingTexturesForPart(_ part:WalkingPart) -> [SKTexture] {
        let textureAtlasName = "\(part.rawValue)\(self.characterType.rawValue)"
        let characterAnimatedAtlas = SKTextureAtlas(named: textureAtlasName)
        var walkFrames = [SKTexture]()
        
        let numImages = characterAnimatedAtlas.textureNames.count
        for i in 1...numImages {
            let characterTextureName = "\(part.rawValue)\(i)\(self.characterType.rawValue)"
            walkFrames.append(characterAnimatedAtlas.textureNamed(characterTextureName))
        }
        return walkFrames
    }
    
    fileprivate func walkMoveEnded() {
        logCharacter("moving ended")
        
        self.currentDirection = .facingUs
        if let closure = self.walkingClosure {
            closure()
        }
    }
    
    internal func walkToLocation(_ location:CGPoint, mode:MoveMode = MoveMode.normal, completion:(()->Void)? = nil) {
        self.walkingClosure = completion
        let location = CGPoint(x: location.x, y: self.position.y)
        var futureDirection: WalkingDirection
        let (moveDuration, moveDifference) = moveDurationAndDifference(self.position, toLocation: location, velocity: mode.rawValue)
        if (moveDifference.x < 0) {
            futureDirection = .left
        } else {
            futureDirection = .right
        }
        
        if (self.action(forKey: kStartWalking) != nil || self.action(forKey: kDoubleStepWalking) != nil || self.action(forKey: kEndWalking) != nil) {
            //still walking
            if self.currentDirection == .facingUs {
                assertionFailure("\(#function) still walking and \"\(self.currentDirection.toString())\"")
            } else if self.currentDirection == futureDirection {
                //keep walking same direction to new position
                self.newWalkSameDirection(location, moveDuration: moveDuration)
                
            } else if self.currentDirection != futureDirection {
                //turn to other direction and walk
                self.newWalkOppositeDirection(location, moveDuration: moveDuration)
                
            }
        } else {
            //standing
            
            if self.currentDirection == .facingUs {
                self.startWalking(direction: futureDirection, closure: {
                    self.midWalkingToLocation(location, moveDuration: moveDuration, completion: {
                        self.endWalking(nil)
                    })
                })
            } else {
                assertionFailure("Standing but not \"\(WalkingDirection.facingUs.toString())\"")
            }
        }
    }
    
    fileprivate func applyDirection(_ direction:WalkingDirection) {
        if direction != .facingUs {
            self.xScale = fabs(self.xScale) * direction.rawValue
        }
        self.currentDirection = direction
    }
    
    fileprivate func newWalkSameDirection(_ location:CGPoint, moveDuration:TimeInterval) {
        if self.action(forKey: kStartWalking) != nil {
            //keep walking texture animation and remove moving action
            self.removeAction(forKey: kMovingAction)
            self.removeAction(forKey: kDoubleStepWalking)
            self.removeAction(forKey: kEndWalking)
            //behind startWalk add new moving and endWalking
            newStuffBlock = {
                self.midWalkingToLocation(location, moveDuration: moveDuration, completion: {
                    self.endWalking(nil)
                })
            }
        } else if self.action(forKey: kDoubleStepWalking) != nil {
            //keep walking texture animation and remove moving and endWalk
            self.removeAction(forKey: kMovingAction)
            self.removeAction(forKey: kEndWalking)
            //setup new moving and endWalking
            self.midWalkingToLocation(location, moveDuration: moveDuration, completion: {
                self.endWalking(nil)
            })
            
        } else if self.action(forKey: kEndWalking) != nil {
            //behind endWalking add new complete walking
            newStuffBlock = {
                self.startWalking(direction: self.currentDirection, closure: { 
                    self.midWalkingToLocation(location, moveDuration: moveDuration, completion: {
                        self.endWalking(nil)
                    })
                })
            }
        }
    }
    
    fileprivate func newWalkOppositeDirection(_ location:CGPoint, moveDuration:TimeInterval) {
        if self.action(forKey: kStartWalking) != nil {
            //remove midWalk and endWalk
            self.removeAction(forKey: kMovingAction)
            self.removeAction(forKey: kDoubleStepWalking)
            self.removeAction(forKey: kEndWalking)
            //when startWalk is finished run endWalk
            newStuffBlock = {
                self.endWalking({
                    //do new walk opposite direction
                    self.startWalking(direction: self.currentDirection.opposite(), closure: {
                        self.midWalkingToLocation(location, moveDuration: moveDuration, completion: {
                            self.endWalking(nil)
                        })
                    })
                })
            }
        } else if self.action(forKey: kDoubleStepWalking) != nil {
            //remove midWalk and endWalk
            self.removeAction(forKey: kMovingAction)
            self.removeAction(forKey: kDoubleStepWalking)
            self.removeAction(forKey: kEndWalking)
            //setup new moving and endWalking
            self.endWalking({
                //do new walk opposite direction
                self.startWalking(direction: self.currentDirection.opposite(), closure: {
                    self.midWalkingToLocation(location, moveDuration: moveDuration, completion: {
                        self.endWalking(nil)
                    })
                })
            })
        } else if self.action(forKey: kEndWalking) != nil {
            //behind endWalking add new complete walking
            newStuffBlock = {
                //do new walk opposite direction
                self.startWalking(direction: self.currentDirection.opposite(), closure: {
                    self.midWalkingToLocation(location, moveDuration: moveDuration, completion: {
                        self.endWalking(nil)
                    })
                })
            }
        }
    }
    
    fileprivate func startWalking(direction: WalkingDirection, closure:@escaping () -> Void) {
        //set direction
        self.applyDirection(direction)
        //do start turn and wait for completion
        
        let startWalkingAnimation = SKAction.animate(with: self.walkingTexturesForPart(.Start), timePerFrame: kTimePerFrame)
        let moveDuration = kTimePerFrame * Double(self.walkingTexturesForPart(.Start).count/2)
        let startWalkingWait = SKAction.wait(forDuration: moveDuration)
        let startWalkingMove = SKAction.moveBy(x: CGFloat(kStartMovingDistance) * direction.rawValue, y: 0, duration: moveDuration)
        let starWalkingMoveSequence = SKAction.sequence([startWalkingWait, startWalkingMove])
        let startWalkingGroup = SKAction.group([startWalkingAnimation, starWalkingMoveSequence])
        
        let startWalkingEnded = SKAction.run({ () -> Void in
            logCharacter("startWalkingEnded")
            if let newStuffBlock = self.newStuffBlock  {
                newStuffBlock()
                self.newStuffBlock = nil
            } else {
                closure()
            }
        })
        
        let startWalkingSequence = SKAction.sequence([startWalkingGroup, startWalkingEnded])
        self.run(startWalkingSequence, withKey: kStartWalking)
    }
    
    fileprivate func midWalkingToLocation(_ location:CGPoint, moveDuration:TimeInterval, completion:@escaping () -> Void) {
        //if not existing prepare mid walking and run in parallel with moving
        if (self.action(forKey: kDoubleStepWalking) == nil) {
            let doubleStepWalkingReapeatedAction = SKAction.repeatForever(SKAction.animate(with: self.walkingTexturesForPart(.Mid), timePerFrame: kTimePerFrame))
            self.run(doubleStepWalkingReapeatedAction, withKey: kDoubleStepWalking)
        }
        
        //assemble moving
        let moveAction = SKAction.move(to: location, duration:(Double(moveDuration)))
        //when moving is done remove doubleStepWalkingReapeatedAction
        let moveDoneAction = SKAction.run{
            self.removeAction(forKey: kDoubleStepWalking)
            
            completion()
        }
        let moveSequence = SKAction.sequence([moveAction, moveDoneAction])
        self.run(moveSequence, withKey: kMovingAction)
    }
    
    fileprivate func endWalking(_ completion:(() -> Void)?) {
        //ending step and turn
        let endWalkingDone = SKAction.run{
            if let newStuffBlock = self.newStuffBlock {
                newStuffBlock()
                self.newStuffBlock = nil
            } else {
                if let completion = completion {
                    completion()
                }
                if (self.action(forKey: kStartWalking) == nil && self.action(forKey: kDoubleStepWalking) == nil) {
                    DispatchQueue.main.async(execute: { 
                        self.walkMoveEnded()
                    })
                }
            }
            
        }
        let endWalkingAction = SKAction.sequence([SKAction.animate(with: self.walkingTexturesForPart(.End), timePerFrame: kTimePerFrame), endWalkingDone])
        self.run(endWalkingAction, withKey: "endWalkingAction")
    }
    
} //class


