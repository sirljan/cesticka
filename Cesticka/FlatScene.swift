//
//  FlatScene.swift
//  Cesticka
//
//  Created by Jan Širl on 09/04/16.
//  Copyright © 2016 Jan Širl. All rights reserved.
//


import SpriteKit

class FlatScene: CustomScene {
    internal var characterType:CharacterType = CharacterType.Girl
    
    fileprivate let kStartAnim = "startAnim"
    fileprivate let kPrepareNode = "prepareNode"
    fileprivate let kBagMinigameVideo = "bagMinigameVideo"
    fileprivate let kTransitionToGameScene = "transitionToGameScene"
    
    //todo make it work with init
//    init?(fileNamed:String, characterType:CharacterType) {
//        characterType = characterType
//        super.childNodeWithName(fileNamed)
//    }
    
    override func didMove(to view: SKView) {
        NSLog("FlatScene didMoveToView")
        
        self.backgroundColor = UIColor.black
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGestureRecognizer(_:)))
        self.view!.addGestureRecognizer(gestureRecognizer)
        
        //Show prepareSpriteNode for time before video is loaded
        let imageName = self.characterType == .Girl ? "girlStill" : "boyStill"
        let prepareSpriteNode = SKSpriteNode(imageNamed: imageName)
        prepareSpriteNode.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2)
        prepareSpriteNode.name = self.kPrepareNode
        prepareSpriteNode.zPosition = 101
        self.addChild(prepareSpriteNode)
        
        self.run(SKAction.wait(forDuration: kAnimationDurationShort), completion: {
            let videoName = "1.StartAnimation\(self.characterType.rawValue)"
            (self.view?.window?.rootViewController as? GameViewController)?.playVideos([videoName], completion:{})
            self.run(SKAction.wait(forDuration: 1), completion: {
                prepareSpriteNode.removeFromParent()
                
                //remove hallway for other character
                let hallWayToRemoveName = self.characterType == .Girl ? "background0Hallway_boy" : "background0Hallway_girl"
                let hallWayToRemove = self.childNode(withName: hallWayToRemoveName)
                hallWayToRemove?.removeFromParent()
                
                let bagToRemoveName = self.characterType == .Girl ? "background0_bag_boy" : "background0_bag_girl"
                let bagToRemove = self.childNode(withName: bagToRemoveName)
                bagToRemove?.removeFromParent()
            })
        }) 
    }
    
    override func update(_ currentTime: TimeInterval) {
    }
    
    @objc func handleTapGestureRecognizer(_ recognizer:UITapGestureRecognizer) {
        if recognizer.state == .ended {
            let videoExitName = self.characterType == .Boy ? "2.ExitAnimation_boy" : "2.ExitAnimation_girl"
            if let bag = self.childNode(withName: kBagMinigameVideo) {
                //skip bag minigame video
                bag.removeFromParent()
            } else {
                //tapping bag or door
                let touchLocationView = recognizer.location(in: recognizer.view)
                let touchLocationScene = self.convertPoint(fromView: touchLocationView)
                
                let center = CGPoint(x: self.frame.width/2, y: self.frame.height/2)
                
                //todo create skspritenode tapable with delegate method when tapped
                let bagName = self.characterType == .Boy ? "background0_bag_boy" : "background0_bag_girl"
                if let bag = self.childNode(withName: bagName) {
                    if bag.frame.contains(touchLocationScene) {
                        bag.run(codeActionSelectionBumpBag, completion: {
                            //run minigame
                            
                            let videoNode = SKVideoNode(fileNamed: "background00_gameplay.mov")
                            videoNode.position = bag.position
                            videoNode.zPosition = 100
                            videoNode.size = self.size
                            videoNode.name = self.kBagMinigameVideo
                            videoNode.setScale(0)
                            videoNode.pause()
                            self.addChild(videoNode)
                            let present = SKAction.group([SKAction.move(to: center, duration: 0.5), SKAction.scale(to: 1, duration: 0.5)])
                            present.timingMode = .easeIn
                            videoNode.run(present, completion: {
                                videoNode.play()
                            })
                        })
                    }
                }
                
                if let door = self.childNode(withName: "flatDoor") {
                    if door.frame.contains(touchLocationScene) {
                        print("door tapped")
                        //bag sound
                        self.run(SKAction.playSoundFileNamed("bag.m4a", waitForCompletion: true))
                        
                        (self.view?.window?.rootViewController as? GameViewController)?.playVideos([videoExitName], completion:{
                            print("Will transition to GameScene")
                            self.transitionToGameScene()
                        })
                        //remove everything besides exitVideo
                        self.run(SKAction.wait(forDuration: 0.5), completion: {
                            for child in self.children {
                                if child.name != videoExitName {
                                    child.removeFromParent()
                                }
                            }
                            
                            let backrgroundWait = SKSpriteNode(imageNamed: "launchicon_menuBlur")
                            backrgroundWait.zPosition = 100
                            backrgroundWait.position = CGPoint(x:self.frame.midX, y:self.frame.midY)
                            let waitSprite = SKSpriteNode(imageNamed: "PleaseWait")
                            waitSprite.name = "PleaseWait"
                            backrgroundWait.zPosition = 101
                            backrgroundWait.position = CGPoint(x:self.frame.midX, y:self.frame.midY)
                            backrgroundWait.addChild(waitSprite)
                            
                            self.addChild(backrgroundWait)
                        })
                    }
                }
            }
        }
    }
    
    func transitionToGameScene() {
        if let scene = GameScene(fileNamed:"LightweightGameScene") {
            scene.characterType = self.characterType
            // Configure the view.
            if let skView = self.view {
                skView.showsFPS = true
                skView.showsNodeCount = true
                
                /* Sprite Kit applies additional optimizations to improve rendering performance */
                skView.ignoresSiblingOrder = true
                
                /* Set the scale mode to scale to fit the window */
                scene.scaleMode = .aspectFit
                let transition = SKTransition.fade(withDuration: 0.66)
                transition.pausesIncomingScene = true
                NSLog("presents GameScene")
                skView.presentScene(scene)
            }
        }
    }
}





