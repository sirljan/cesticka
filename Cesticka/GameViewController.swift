//
//  GameViewController.swift
//  Cesticka
//
//  Created by Jan Širl on 08/03/16.
//  Copyright (c) 2016 Jan Širl. All rights reserved.
//

import UIKit
import SpriteKit

import AVKit
import AVFoundation

enum AppError : Error {
    case invalidResource(String, String)
}

class GameViewController: UIViewController {
    var hasPlayer = false
    var videoClosure:(()->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let scene = MenuScene(fileNamed:"MenuScene") {
//        if let scene = GameScene(fileNamed:"GameScene") {
//                    if let scene = SKScene(fileNamed: "Scene1_city1") {
//        if let scene = SKScene(fileNamed: "Scene7_mill") {
            // Configure the view.
            let skView = self.view as! SKView
            skView.showsFPS = true
            skView.showsNodeCount = true
            
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView.ignoresSiblingOrder = true
            
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .aspectFit
            
            skView.presentScene(scene)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.hasPlayer, let closure = self.videoClosure {
            self.hasPlayer = false
            closure()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func playVideos(_ names:[String], type:String = "mov", completion:(()->Void)?) {
        self.videoClosure = completion
        do {
            try playVideoItems(names, type: type)
        } catch AppError.invalidResource(let name, let type) {
            debugPrint("Could not find resource \(name).\(type)")
        } catch {
            debugPrint("Generic error")
        }
    }
    
    fileprivate func playVideoItems(_ names:[String], type:String) throws {
        print("GameViewController.playVideoItem")
        
        var playerItems = [AVPlayerItem]()
        for name in names {
            guard let path = Bundle.main.path(forResource: name, ofType:type) else {
                throw AppError.invalidResource(name, type)
            }
            let playerItem = AVPlayerItem(url: URL(fileURLWithPath: path))
            playerItems.append(playerItem)
        }
        let playerQueue = AVQueuePlayer(items: playerItems)
        
        let playerController = AVPlayerViewController()
        playerController.player = playerQueue
        playerController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
        self.hasPlayer = true
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd(_:)), name:NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItems.last)
        
        self.present(playerController, animated: true) {
            playerQueue.play()
        }
    }
    
    @objc func playerItemDidReachEnd(_ playerItem:AVPlayerItem) {
        NotificationCenter.default.removeObserver(self)
        if let playerViewController = self.presentedViewController as? AVPlayerViewController {
            playerViewController.dismiss(animated: true, completion: { 
                if self.hasPlayer, let closure = self.videoClosure {
                    self.hasPlayer = false
                    closure()
                }
            })
        }
    }
    
    override var shouldAutorotate : Bool {
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden : Bool {
        return true
    }
}
