//
//  CodeActions.swift
//  Cesticka
//
//  Created by Jan Širl on 10/04/16.
//  Copyright © 2016 Jan Širl. All rights reserved.
//

import SpriteKit

let codeActionSelectionBumpRelative = SKAction.sequence([SKAction.scale(by: 1.2, duration: 0.1), SKAction.scale(by: 0.8, duration: 0.1)])
let codeActionSelectionBump = SKAction.sequence([SKAction.scale(to: 1.2, duration: 0.1), SKAction.scale(by: 1, duration: 0.1)])
let codeActionSelectionBumpBag = SKAction.sequence([SKAction.scale(to: 0.75, duration: 0.1), SKAction.scale(to: 0.7, duration: 0.1)])

let moveByX:CGFloat = 1450
let moveByY:CGFloat = 70
let velocity:CGFloat = 60
let codeActionTractorMoveRight = SKAction.moveBy(x: moveByX, y: 0, duration: Double(moveByX/velocity))

let codeActionTractorMoveDown = SKAction.moveBy(x: 0, y: -moveByY, duration: 1)
let codeActionTractorFlipLeft = SKAction.scaleX(to: -1, duration: 1)
let codeActionTractorTurnLeft = SKAction.group([codeActionTractorMoveDown, codeActionTractorFlipLeft])

let codeActionTractorMoveLeft = SKAction.moveBy(x: -moveByX, y: 0, duration: Double(moveByX/velocity))

let codeActionTractorScaleDown = SKAction.scaleX(to: 0.3, duration: 2)
let codeActionTractorMoveUp = SKAction.moveBy(x: 0, y: moveByY, duration: 2)
let codeActionTractorFlipRight = SKAction.scaleX(to: 1, duration: 1)
let codeActionTractorTurnRight = SKAction.sequence([codeActionTractorScaleDown, codeActionTractorMoveUp, codeActionTractorFlipRight])

let codeActionTractorMovingRepeated = SKAction.repeatForever(SKAction.sequence([codeActionTractorMoveRight, codeActionTractorTurnLeft, codeActionTractorMoveLeft, codeActionTractorTurnRight]))

