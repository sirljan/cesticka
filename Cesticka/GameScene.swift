//
//  GameScene.swift
//  Cesticka
//
//  Created by Jan Širl on 08/03/16.
//  Copyright (c) 2016 Jan Širl. All rights reserved.
//

//Z levels
//sky           -30
//clouds        -20
//background    0
//buildings     20
//character     50
//before car.   60
//water         100

import SpriteKit

import AVKit
import AVFoundation

enum SceneExchange:CGFloat {
    case goingLeft = -1
    case goingRight = 1
    case stayInScene = 0
}

enum Scenes : Int {
    case city1 = 0
    case city2
    case reservoir
    case farmHouse
    case ruin
    case rangerHouse
    case mill
    case church
    case townSquare
    case school
    case cottage
    
    func toCGFloat() -> CGFloat {
        return CGFloat(self.rawValue)
    }
}

let kCharacterNodeName = "character"
let kCharacterToNextScene:CGFloat = 0.14

let kDebugMusicOn = true
let kDebugStartUpScene:Scenes = Scenes.city1

let kAnimationDurationShort = 0.33
let kAnimationDurationMid = 0.66
let kAnimationDurationLong = 0.99

func moveDurationAndDifference(_ fromLocation:CGPoint, toLocation:CGPoint, velocity:CGFloat) -> (moveDuration:TimeInterval, moveDifference:CGPoint) {
    let moveDifference = CGPoint(x: toLocation.x - fromLocation.x, y: toLocation.y - fromLocation.y)
    let distanceToMove = sqrt(moveDifference.x * moveDifference.x + moveDifference.y * moveDifference.y)
    let moveDuration = TimeInterval(distanceToMove / velocity)
    return (moveDuration, moveDifference)
}

class GameScene: CustomScene {
    internal var characterType:CharacterType = .Girl
    fileprivate var character: Character!
    //scene (enviroment) currently visible
    var currentSceneIndex:CGFloat = 0
    var currentScenePoint:CGPoint {
        get {
            return CGPoint(x: self.frame.width * self.currentSceneIndex, y: 0)
        }
    }
    var currentSceneCenterPoint:CGPoint {
        get {
            return CGPoint(x: self.currentScenePoint.x + self.frame.width/2, y: self.frame.height/2)
        }
    }
    
    fileprivate let kMusicAudioNode = "musicAudioNode"
    fileprivate let kBusSound       = "busDrive.mp3"
    fileprivate let kSchoolNode     = "school"
    fileprivate let kSchoolAnimation = "schoolAnimation"
    fileprivate let kHorse          = "horse"
    fileprivate let kCakeSignHighlighted = "SignHighlight"
    fileprivate let kTractor        = "tractor"
    
    fileprivate var horseInitialPosition:CGPoint?
    
    let scenesExecutes:[(String, String?)] =
        [("Scene1_city1"     ,"trafficLight"        ),
         ("Scene2_city2"     ,"busToLeft"           ),
         ("Scene3_reservoir" ,"background03_Fish"   ),
         ("Scene4_farmHouse" ,"horse"               ),
         ("Scene5_ruin"      ,"ruinStartMinigame"   ),
         ("Scene6_rangerHouse","rangerHouse"        ),
         ("Scene7_mill"      ,"tractor"             ),
         ("Scene8_church"    ,"church"              ),
         ("Scene9_townSquare","sugarShop"           ),
         ("Scene10_school"   ,"school"              ),
         ("Scene11_cottage"  ,nil        )]
    
    
    
    override func didMove(to view: SKView) {
        print("GameScene didMoveToView")
        
        //initail setup
        self.currentSceneIndex = CGFloat(kDebugStartUpScene.rawValue)
        let startUpPosition = self.frame.width * kDebugStartUpScene.toCGFloat()
        self.camera?.position = CGPoint(x: startUpPosition + self.frame.width/2, y: self.frame.height/2)
        
        self.character = Character(characterType: self.characterType, startupOffsetX: startUpPosition)
        self.addChild(self.character)
        
        self.childNode(withName: "//\(kCakeSignHighlighted)")?.isHidden = true
        let tractor = self.childNode(withName: "//\(kTractor)")
        tractor?.run(codeActionTractorMovingRepeated)
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(GameScene.handleTapGestureRecognizer(_:)))
        self.view!.addGestureRecognizer(gestureRecognizer)
        
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(GameScene.handleDoubleTapGestureRecognizer(_:)))
        doubleTap.numberOfTapsRequired = 2
        self.view!.addGestureRecognizer(doubleTap)
        
        //Clouds setup
        self.setupClouds()
        
        //sounds
        self.updateSound()

        let loadScene:(_ sceneName:String, _ index:Int)->Void = { sceneName, index in
            if let path = Bundle.main.path(forResource: sceneName, ofType: "sks"), let width = self.scene?.frame.width {
                let nextScene = SKReferenceNode (url: URL (fileURLWithPath: path))
                nextScene.position = CGPoint(x: CGFloat(index) * width, y: 0)
                self.scene?.addChild(nextScene)
                print("\(sceneName) loaded")
                self.horseInitialPosition = self.childNode(withName: "//\(self.kHorse)")?.position
            }
        }
        
        for (index, (sceneName,_)) in self.scenesExecutes.enumerated() {
            self.run(SKAction.wait(forDuration: Double(index*3)), completion: {
                loadScene(sceneName, index)
            })
        }
    }
    
    func setupClouds() {
        //todo codesmell, code as data, put this into plist
        self.enumerateChildNodes(withName: "//*cloud*fast") { (cloud, boolPointer) in
            if let cloud = cloud as? Cloud {
                cloud.velocity = 10
            }
        }
        self.enumerateChildNodes(withName: "//*cloud*medium") { (cloud, boolPointer) in
            if let cloud = cloud as? Cloud {
                cloud.velocity = 20
            }
        }
        self.enumerateChildNodes(withName: "//*cloud*slow") { (cloud, boolPointer) in
            if let cloud = cloud as? Cloud {
                cloud.velocity = 30
            }
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        //clouds
        self.enumerateChildNodes(withName: "//*[Cc]loud*") { (cloud, boolPointer) in
            if let cloud = cloud as? Cloud {
                cloud.updateMovement()
            }
        }
        
        //update cars' and buses' sounds
        self.enumerateChildNodes(withName: "//busToLeft") { (bus, boolPointer) in
            if bus.childNode(withName: self.kBusSound) == nil && (Int(self.currentSceneIndex) == Scenes.city1.rawValue || Int(self.currentSceneIndex) == Scenes.city2.rawValue) {
                let busSound = SKAudioNode(fileNamed: self.kBusSound)
                busSound.name = self.kBusSound
                bus.addChild(busSound)
            } else {
                let distance = fabs(self.character.position.x - bus.position.x) > 1024 ? 1024 : fabs(self.character.position.x - bus.position.x)
                if distance <= 1024 {
                    bus.childNode(withName: self.kBusSound)?.run(SKAction.changeVolume(to: Float((1024/distance) - 1)*0.01, duration: 0))
                }
            }
        }
    }
    
    func updateSound() {
        if kDebugMusicOn {
            let musicIndex:Int = Int(self.currentSceneIndex.truncatingRemainder(dividingBy: 3))+1
            let music = SKAudioNode(fileNamed: "cesticka\(musicIndex).mp3")
            music.name = kMusicAudioNode
            music.run(SKAction.changeVolume(to: 0, duration: 0))
            
            //Fade out is not working
            //https://forums.developer.apple.com/thread/44637
            let durationFade = 3.0
            let fadeIn = SKAction.changeVolume(to: 0.4, duration: durationFade)
            fadeIn.timingMode = .easeInEaseOut
            if let previousAudio = self.childNode(withName: kMusicAudioNode) {
                let fadeOut = SKAction.changeVolume(to: 0, duration: durationFade)
                fadeOut.timingMode = .easeInEaseOut
                previousAudio.run(fadeOut, completion: {
                    previousAudio.removeFromParent()
                })
                self.addChild(music)
                music.run(fadeIn)
            } else {
                self.addChild(music)
                music.run(fadeIn)
            }
        }
    }
    
    /** camera updates only when character is on the sides to go next or previous page
     */
    func updateCamera() {
        if let camera = camera {
            let currentSceneOffset = self.currentSceneIndex * self.frame.width
            let goingLeft:Bool = character.position.x < (currentSceneOffset + (self.frame.size.width * kCharacterToNextScene))
            let goingRight:Bool = character.position.x > (currentSceneOffset + (self.frame.size.width * (1-kCharacterToNextScene)))
            let futureSceneIndex = goingLeft ? self.currentSceneIndex-1 : self.currentSceneIndex+1
            let futureSceneOffset = futureSceneIndex * self.frame.width
            
            if Int(futureSceneIndex) >= 0 && Int(futureSceneIndex) < self.scenesExecutes.count {
                
                if goingLeft || goingRight {
                    
                    //run walking that direction with camera attach to current relative position to the character
                    let cameraDestX = goingLeft ? currentSceneOffset - self.frame.width/2 : futureSceneOffset + (self.frame.width/2)
                    let cameraDestination = CGPoint(x: cameraDestX, y: camera.position.y)
                    let (moveDuration, _) = moveDurationAndDifference(camera.position, toLocation: cameraDestination, velocity: 2*character.velocity.rawValue)
                    let moveCameraToNextSceneCenter = SKAction.move(to: cameraDestination, duration: moveDuration)
                    moveCameraToNextSceneCenter.timingMode = .easeInEaseOut
                    camera.run(moveCameraToNextSceneCenter, completion: {
                        print("Walked to \(goingLeft ? "previous" : "next") scene")
                        self.currentSceneIndex = goingLeft ? self.currentSceneIndex-1 : self.currentSceneIndex+1
                        self.didMoveToNewScene()
                    })
                    if Int(futureSceneIndex) != Scenes.cottage.rawValue {
                        let characterXDestination = goingLeft ? currentSceneOffset + (self.frame.width * -kCharacterToNextScene) : currentSceneOffset + (self.frame.width * (1+kCharacterToNextScene))
                        character.walkToLocation(CGPoint(x: characterXDestination, y: character.position.y))
                    }
                }
            }
        }
    }
    
    func didMoveToNewScene() {
        self.updateSound()
        
        switch Int(self.currentSceneIndex) {
        case Scenes.city2.rawValue:
            if let busSound = self.childNode(withName: "//\(kBusSound)") {
                busSound.isPaused = false
            }
        case Scenes.reservoir.rawValue:
            if let busSound = self.childNode(withName: "//\(kBusSound)") {
                busSound.removeFromParent()
            }
            self.resetHorse()
        case Scenes.farmHouse.rawValue:
            if let horseAction = SKAction(named: "horseAction2"), let horse = self.childNode(withName: "//horse") {
                horse.run(horseAction)
            }
        case Scenes.ruin.rawValue:
            self.resetHorse()
        case Scenes.school.rawValue:
            if let school = self.childNode(withName: "//\(kSchoolNode)"), let schoolAnimation = SKAction(named: kSchoolAnimation) {
                school.run(schoolAnimation)
            }
        case Scenes.cottage.rawValue:
            let name = "4.finale\(self.characterType.rawValue)"
            self.playVideoNode(self.currentSceneCenterPoint, size: self.frame.size, name: name, completion: { 
                self.character.position = CGPoint(x: self.currentScenePoint.x + 550, y: kCharStartUpOffset.y)
                self.childNode(withName: "//\(name)")?.removeFromParent()
            })
        default: break
        }
    }
    
    func resetHorse() {
        if let horse = self.childNode(withName: "//\(kHorse)"), let position = self.horseInitialPosition {
            horse.position = position
            horse.removeAllActions()
        }
    }
    
    /**
     Runs minigame if user tap executable node
     - returns: true when walkNeeded
     */
    fileprivate func runMinigame(_ touchLocationScene:CGPoint) -> Bool {
        let number = NSString(format: "%02d", Int(self.currentSceneIndex+1))
        let videoName = "background\(number)_gameplay"
        if Int(self.currentSceneIndex) >= 0 && Int(self.currentSceneIndex) < self.scenesExecutes.count {
            if let executeNodeName = self.scenesExecutes[Int(self.currentSceneIndex)].1 {
                if let executeNode = self.childNode(withName: "//\(executeNodeName)") {
                    let touchLocationInParentNode = self.convert(touchLocationScene, to: executeNode.parent!)
                    if executeNode.contains(touchLocationInParentNode) {
                        let videoBlock:(_ completion:(()->Void)?) -> Void = { completion in
                            print("Execution node \(executeNodeName) will play \(videoName)")
                            self.isPaused = true
                            (self.view?.window?.rootViewController as? GameViewController)?.playVideos([videoName], completion:(completion == nil ? self.videoDidEndPlaying : completion))
                        }
                        
                        if currentSceneIndex == Scenes.townSquare.toCGFloat() {
                            debugPrint("Walk before sugar shop.")
                            
                            //highlight cake sign
                            if let cakeSign = self.childNode(withName: "//\(kCakeSignHighlighted)") {
                                cakeSign.alpha = 0
                                cakeSign.isHidden = false
                                let lightUp = SKAction.fadeAlpha(to: 1, duration: kAnimationDurationShort)
                                lightUp.timingMode = .easeInEaseOut
                                
                                cakeSign.run(SKAction.sequence([lightUp, lightUp.reversed()]), completion:{
                                    cakeSign.isHidden = true
                                })
                            }
                            
                            self.character.walkToLocation(touchLocationScene, completion: {
                                let videoCandyEntry = "3.EnterCandyshop1\(self.characterType.rawValue)"
                                let videoCandyExit = "3.ExitCandyshop1\(self.characterType.rawValue)"
                                (self.view?.window?.rootViewController as? GameViewController)?.playVideos([videoCandyEntry, videoName, videoCandyExit], completion: self.videoDidEndPlaying)
                            })
                            return false
                        } else {
                            videoBlock(nil)
                            return true
                        }
                    }
                }
            }
        }
        return true
    }
    
    //When playing video over AVPlayerVC from GameViewController
    func videoDidEndPlaying() {
        print("videoDidEndPlaying")
        self.isPaused = false
    }
    
    @objc func handleTapGestureRecognizer(_ recognizer:UITapGestureRecognizer) {
        if recognizer.state == .ended {
            let touchLocationView = recognizer.location(in: recognizer.view)
            let touchLocationScene = self.convertPoint(fromView: touchLocationView)

            let walkNeeded = self.runMinigame(touchLocationScene)
            
            if walkNeeded {
                self.character.walkToLocation(touchLocationScene, completion: {
                    self.updateCamera()
                })
            }
        }
    }
    
    @objc func handleDoubleTapGestureRecognizer(_ recognizer:UITapGestureRecognizer) {
        if recognizer.state == .ended {
            let touchLocationView = recognizer.location(in: recognizer.view)
            let touchLocationScene = self.convertPoint(fromView: touchLocationView)
            
            self.character.walkToLocation(touchLocationScene, mode: MoveMode.fast, completion: {
                self.updateCamera()
            })
        }
    }
}
